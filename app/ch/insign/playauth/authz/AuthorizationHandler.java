package ch.insign.playauth.authz;

import org.apache.shiro.authz.AuthorizationException;
import play.mvc.Http;
import play.mvc.Result;

/**
 * AuthorizationHandler handles authorization and authentication exceptions.
 */
public interface AuthorizationHandler {

    /**
     * Event is triggered when operation or action is not allowed.
     *
     * @param ctx Current Http context
     * @param e Authorization exception
     * @return
     */
    Result onUnauthorized(Http.Context ctx, AuthorizationException e);

    /**
     * Event is triggered when attempting to execute an authorization action when a successful
     * authentication hasn't yet occurred.
     *
     * @param ctx Current Http context
     * @param e Authorization exception
     * @return
     */
    Result onUnauthenticated(Http.Context ctx, AuthorizationException e);
}
