package ch.insign.playauth.shiro.env;

import ch.insign.playauth.shiro.mgt.PlayShiroSecurityManager;
import org.apache.shiro.env.Environment;

public interface PlayShiroEnvironment extends Environment {
    PlayShiroSecurityManager getPlayShiroSecurityManager();
}
