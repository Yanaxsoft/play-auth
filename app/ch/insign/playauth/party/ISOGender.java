package ch.insign.playauth.party;

public enum ISOGender {

    MALE,

    FEMALE,

    /** The gender is not known */
    NOTKNOWN,

    /** The person has choosen not to specify his or her gender */
    NOTSPECIFIED
}
