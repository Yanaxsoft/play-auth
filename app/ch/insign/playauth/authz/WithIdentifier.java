package ch.insign.playauth.authz;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface WithIdentifier {
	Class<? extends Identifier> value();
}
