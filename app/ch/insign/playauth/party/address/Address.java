package ch.insign.playauth.party.address;

import java.io.Serializable;

abstract public class Address implements Serializable {

    private static final long serialVersionUID = 1L;

    protected String address;

    public Address(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    @Override
    public String toString() {
        return getAddress();
    }
}
