package ch.insign.playauth.realm;

import ch.insign.playauth.PlayAuth;
import ch.insign.playauth.authc.ExpirableAuthenticationInfo;
import ch.insign.playauth.authc.LockableAuthenticationInfo;
import ch.insign.playauth.party.Party;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.shiro.authc.*;
import org.apache.shiro.realm.AuthenticatingRealm;
import org.apache.shiro.subject.PrincipalCollection;

import java.util.Optional;

public class DefaultRealm extends AuthenticatingRealm {

	public static final String REALM_NAME = "default";

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {

        Optional<AuthenticationInfo> authInfo = findAuthenticationInfo(token);

	    authInfo.ifPresent(info -> {
            if (info instanceof LockableAuthenticationInfo) {
                if (((LockableAuthenticationInfo) info).isLocked()) {
                    throw new LockedAccountException("Account [" + info + "] is locked.");
                }
            }

            if (info instanceof ExpirableAuthenticationInfo) {
                if (((ExpirableAuthenticationInfo) info).isCredentialsExpired()) {
                    String msg = "The credentials for account [" + info + "] are expired";
                    throw new ExpiredCredentialsException(msg);
                }
            }
	    });

        return authInfo.orElse(null);
    }

	private Optional<AuthenticationInfo> findAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
		return Optional.ofNullable(PlayAuth.getPartyManager().findOneByPrincipal(token.getPrincipal()))
				.map(DefaultRealm::buildAuthenticationInfo);
	}

	private static AuthenticationInfo buildAuthenticationInfo(Party p) {
		return new LockableAuthenticationInfo() {
			@Override
			public PrincipalCollection getPrincipals() {
				return p.getPrincipals();
			}

			@Override
			public Object getCredentials() {
				return p.getCredentials();
			}

			@Override
			public boolean isLocked() {
				return p.isLocked();
			}

			@Override
			public void setLocked(boolean locked) {
				p.setLocked(locked);
			}

			@Override
			public String toString() {
				return new ToStringBuilder(this)
						.append("principals", getPrincipals())
						.append("isLocked", isLocked())
						.toString();
			}
		};
	}
}
