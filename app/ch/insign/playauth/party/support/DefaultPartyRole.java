package ch.insign.playauth.party.support;


import ch.insign.playauth.party.PartyRole;
import play.data.validation.Constraints;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "auth_default_party_role")
public class DefaultPartyRole implements PartyRole {

    @Id
    @GeneratedValue
    private long id;

    /*
     * Field required to cascade deleting the role.
     */
    @ManyToMany(mappedBy = "partyRoles", cascade = CascadeType.REMOVE)
    private Set<DefaultParty> parties = new HashSet<>();

    /*
     * Predefined Roles
     */
    public static final String ROLE_SUPERUSER = "Superuser";
    public static final String ROLE_ADMIN = "Admin";
    public static final String ROLE_USER = "User";

    @Constraints.Required
    @Column(name = "name", nullable = false, unique = true)
    private String name;

    public DefaultPartyRole() {
    }

    public DefaultPartyRole(String name) {
        this.name = name;
    }

    /**
     * Get the string representation of the id.
     *
     * @return
     */
    public String getId() {
        if (id == 0) return null;
        return String.valueOf(id);
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return getName();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 17;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		DefaultPartyRole that = (DefaultPartyRole) o;
		return name.equals(that.name);
	}
}
