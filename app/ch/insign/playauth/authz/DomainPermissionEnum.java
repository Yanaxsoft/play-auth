package ch.insign.playauth.authz;

/**
 * Default implementation of the {@code DomainPermission} interface to
 * allow clean and unobtrusive definition of the {@code DomainPermission} enum constants.
 *
 *<pre>
 * {@code
  *     public enum FilePermission implements DomainPermissionEnum<File> {
 *         READ, WRITE, EXECUTE, DELETE;
 *     }
 * }
 * </pre>
 */
@FunctionalInterface
public interface DomainPermissionEnum<T> extends DomainPermission<T> {

	String name();

	@Override
	default String domain() {
		return DomainPermissionUtils.getTargetType(this)
				.map(Class::getName)
				.orElseThrow(() -> new UnknownDomainException("Cannot resolve permission domain."));
	}

	@Override
	default ObjectIdentity target() {
		return ObjectIdentity.ALL;
	}

	/**
	 * Signals that the target type {@code Class<T>} represented by the type argument 'T'
	 * could not be retrieved because of the Java type erasure or some other reason.
	 */
	public static class UnknownDomainException extends RuntimeException {
		public UnknownDomainException(String message) {
			super(message);
		}
	}
}
