package ch.insign.playauth.authz.support;

import ch.insign.playauth.authz.AuthorizationHandler;
import org.apache.shiro.authz.AuthorizationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.mvc.Http.Context;
import play.mvc.Result;
import play.mvc.Results;

public class DefaultAuthorizationHandler implements AuthorizationHandler {
	private final static Logger logger = LoggerFactory.getLogger(DefaultAuthorizationHandler.class);

    @Override
    public Result onUnauthorized(Context ctx, AuthorizationException e) {
        return Results.forbidden(views.html.defaultpages.unauthorized.render());
    }

    @Override
    public Result onUnauthenticated(Context ctx, AuthorizationException e) {
        return Results.unauthorized(views.html.defaultpages.unauthorized.render());
    }

}
