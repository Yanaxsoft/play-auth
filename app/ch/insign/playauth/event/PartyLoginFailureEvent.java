package ch.insign.playauth.event;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationToken;

public class PartyLoginFailureEvent implements EventObject {

    private final AuthenticationToken token;
    private final AuthenticationException authenticationException;

    public PartyLoginFailureEvent(AuthenticationToken token, AuthenticationException authenticationException) {
        this.token = token;
        this.authenticationException = authenticationException;
    }

    public AuthenticationToken getToken() {
        return token;
    }

    public AuthenticationException getAuthenticationException() {
        return authenticationException;
    }
}
