package ch.insign.playauth.authz;

import org.apache.commons.lang3.Validate;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class DomainActionEntry implements Serializable {

	public static final DomainActionEntry ALL = new DomainActionEntry(DomainPermission.ALL.domain(), DomainPermission.ALL.name());

	private String domain;
	private String name;

	protected DomainActionEntry() { }

	public DomainActionEntry(DomainPermission<?> permission) {
		this(permission.domain(), permission.name());
	}

	public DomainActionEntry(String domain, String name) {
		Validate.notBlank(domain);
		Validate.notBlank(name);

		this.domain = domain;
		this.name = domain.equals(DomainPermission.ALL.domain()) ? DomainPermission.ALL.name() : name;
	}

	public String domain() {
		return domain;
	}

	public String name() {
		return name;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof DomainActionEntry)) return false;

		DomainActionEntry that = (DomainActionEntry) o;

		if (!domain.equals(that.domain)) return false;
		if (!name.equals(that.name)) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = domain.hashCode();
		result = 31 * result + name.hashCode();
		return result;
	}
}
