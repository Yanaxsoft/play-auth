package ch.insign.playauth.authc;

import ch.insign.playauth.PlayAuth;
import ch.insign.playauth.event.PartyLoginFailureEvent;
import ch.insign.playauth.event.PartyLoginSuccessEvent;
import ch.insign.playauth.event.PartyLogoutEvent;
import ch.insign.playauth.party.Party;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.subject.PrincipalCollection;

public class AuthenticationListener implements org.apache.shiro.authc.AuthenticationListener {

    @Override
    public void onSuccess(AuthenticationToken token, AuthenticationInfo info) {
        Party party = PlayAuth.getPartyManager().findOneByPrincipals(info.getPrincipals());
        PlayAuth.getEventDispatcher().dispatch(new PartyLoginSuccessEvent(token, party));
    }

    @Override
    public void onFailure(AuthenticationToken token, AuthenticationException ae) {
        PlayAuth.getEventDispatcher().dispatch(new PartyLoginFailureEvent(token, ae));
    }

    @Override
    public void onLogout(PrincipalCollection principals) {
        Party party = PlayAuth.getPartyManager().findOneByPrincipals(principals);
        PlayAuth.getEventDispatcher().dispatch(new PartyLogoutEvent(party));
    }

}
