package ch.insign.playauth.authz;

import java.util.Objects;

/**
 * The DomainPermission implementation that retains a target type.
 */
public class TypedDomainPermission<T> extends DefaultDomainPermission<T> {
	private final Class<T> targetType;

	public TypedDomainPermission(Class<T> domainType, String name) {
		this(domainType.getName(), name, ObjectIdentity.ALL, domainType);
	}

	public TypedDomainPermission(String domain, String name, ObjectIdentity target, Class<T> targetType) {
		super(domain, name, target);
		Objects.requireNonNull(targetType);
		this.targetType = targetType;
	}

	public Class<T> targetType() {
		return targetType;
	}
}
