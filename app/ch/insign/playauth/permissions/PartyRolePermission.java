package ch.insign.playauth.permissions;

import ch.insign.playauth.authz.DomainPermissionEnum;
import ch.insign.playauth.party.PartyRole;


public enum PartyRolePermission implements DomainPermissionEnum<PartyRole> {
	BROWSE,
	READ,
	EDIT,
	ADD,
	DELETE,
	GRANT,
	REVOKE;
}
