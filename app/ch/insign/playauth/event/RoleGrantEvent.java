package ch.insign.playauth.event;

import ch.insign.playauth.party.Party;
import ch.insign.playauth.party.PartyRole;

public class RoleGrantEvent extends RoleAuthorizationEvent {

	public RoleGrantEvent(PartyRole role, Party party) {
		super(role, party);
	}
}
