package ch.insign.playauth.event;

import ch.insign.playauth.authz.DomainPermission;
import ch.insign.playauth.authz.SecurityIdentity;

public class PermissionAllowEvent extends PermissionAuthorizationEvent {

	public PermissionAllowEvent(SecurityIdentity sid, DomainPermission<?> permission) {
		super(sid, permission);
	}
}
