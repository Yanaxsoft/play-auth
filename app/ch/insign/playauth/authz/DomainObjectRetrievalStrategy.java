package ch.insign.playauth.authz;


public interface DomainObjectRetrievalStrategy {
	Object getSidObject(AccessControlEntry ace) throws UnknownDomainClassException ;
	Object getOidObject(AccessControlEntry ace) throws UnknownDomainClassException ;

	/**
	 * Signals that the DomainObjectRetrievalStrategy doesn't know how to retrieve an object
	 * for given class name and identifier from AccessControlEntry object
	 */
	public static class UnknownDomainClassException extends Exception {
		public UnknownDomainClassException(String message) {
			super(message);
		}
	}
}
