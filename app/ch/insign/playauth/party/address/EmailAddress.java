package ch.insign.playauth.party.address;

public class EmailAddress extends Address {

    private static final long serialVersionUID = 1L;

    public EmailAddress(String email) {
        super(email);
    }
}
