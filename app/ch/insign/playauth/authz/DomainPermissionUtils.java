package ch.insign.playauth.authz;

import org.apache.commons.lang3.reflect.TypeUtils;

import java.util.Optional;

public class DomainPermissionUtils {

	/**
	 * Returns a base target type of the given DomainPermission.
	 */
	@SuppressWarnings("unchecked")
	public static <T> Optional<Class<T>> getTargetType(DomainPermission<T> p) {
		return Optional.ofNullable((Class < T >) TypeUtils.getRawType(DomainPermission.class.getTypeParameters()[0], p.getClass()));
	}

}
