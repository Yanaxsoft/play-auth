package ch.insign.playauth.shiro.util;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import play.mvc.Http;

public interface HttpContextSource {
    Http.Context getHttpContext();
}
