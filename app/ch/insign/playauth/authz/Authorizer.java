package ch.insign.playauth.authz;

import java.io.Serializable;

public interface Authorizer extends Serializable {
	boolean isPermitted(Object authority, DomainPermission<?> permission);
}
