package ch.insign.playauth.authz;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface WithHierarchy {
	Class<? extends Hierarchy> value();
}
