package ch.insign.playauth.party;

import ch.insign.playauth.party.preference.Preference;
import ch.insign.playauth.party.preference.PreferenceOption;
import ch.insign.playauth.party.preference.PreferenceType;
import org.apache.shiro.subject.PrincipalCollection;

import java.util.Set;

public interface Party {

	String getId();

    String getName();

	String getEmail();

	PrincipalCollection getPrincipals();

	Object getCredentials();

	boolean isLocked();

	void setLocked(boolean locked);

    Set<PartyRole> getRoles();

    void addRole(PartyRole r);

    void removeRole(PartyRole r);

    boolean hasRole(PartyRole r);

    Set<Preference> getPreferences();

    Preference getPreference(PreferenceType t);

    Preference getPreference(String type);

    void setPreference(Preference p);

	void setPreference(PreferenceType t, PreferenceOption o);

    void setPreference(String type, String option);
}