package ch.insign.playauth.party.preference;

import javax.persistence.CascadeType;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@Embeddable
public class Preference {

    @ManyToOne(cascade = CascadeType.ALL)
    private PreferenceType type;

    @ManyToOne(cascade = CascadeType.ALL)
    private PreferenceOption option;

    protected Preference() {

    }

    public Preference(PreferenceType type, PreferenceOption option) {
        this.type = type;
        this.option = option;
    }

    public String getOptionName() {
        return option.getName();
    }

    public String getOptionDescription() {
        return option.getDescription();
    }

    public PreferenceType getType() {
        return type;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31)
                .append(type)
                .append(option)
                .toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        Preference other = (Preference) obj;
        return new EqualsBuilder()
                .append(type, other.type)
                .append(option, other.option)
                .isEquals();
    }

    @Override
    public String toString() {
        return type.getName() + "(" + option.getName() + ")";
    }
}
