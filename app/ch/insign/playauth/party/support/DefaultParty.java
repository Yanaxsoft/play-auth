package ch.insign.playauth.party.support;


import ch.insign.playauth.event.RoleGrantEvent;
import ch.insign.playauth.event.RoleRevokeEvent;
import ch.insign.playauth.party.Party;
import ch.insign.playauth.party.PartyIdentifier;
import ch.insign.playauth.party.PartyRole;
import ch.insign.playauth.party.address.EmailAddress;
import ch.insign.playauth.party.preference.Preference;
import ch.insign.playauth.party.preference.PreferenceOption;
import ch.insign.playauth.party.preference.PreferenceType;
import ch.insign.playauth.realm.DefaultRealm;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.SimplePrincipalCollection;
import play.data.validation.Constraints;
import play.db.jpa.JPA;

import javax.persistence.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static ch.insign.playauth.PlayAuth.getEventDispatcher;

@Entity
@Table(name = "auth_party")
@Inheritance (strategy=InheritanceType.SINGLE_TABLE)
public class DefaultParty implements Party  {

    @Id
    @GeneratedValue
    private long id;

    @Constraints.Required
    @Constraints.Email
    @Column(unique = true)
    public String email;

    public String name;

    public String credential;

    public boolean isLocked = false;

    @Transient
    public String password;

    @Transient
    public String repeatPassword;

    @ManyToMany
    @JoinTable(
            name = "auth_default_party_default_role",
            joinColumns = @JoinColumn(name = "USER_ID", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(name = "ROLE_ID", referencedColumnName = "ID"))
    private Set<DefaultPartyRole> partyRoles = new HashSet<>();

    @ElementCollection(fetch = FetchType.EAGER, targetClass = Preference.class)
    @CollectionTable(name = "auth_party_preferences")
    @OneToMany(cascade = CascadeType.ALL)
    @MapKey(name = "type")
    private Map<PreferenceType, Preference> preferences = new HashMap<PreferenceType, Preference>();

    @Override
    public String getId() {
        return String.valueOf(id);
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Object getCredentials() {
        return this.credential;
    }

    public void setCredentials(String credentials) {
        this.credential = credentials;
    }

    public String getRepeatPassword() {
        return repeatPassword;
    }

    public void setRepeatPassword(String repeatPassword) {
        this.repeatPassword = repeatPassword;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public PrincipalCollection getPrincipals() {
		SimplePrincipalCollection principals = new SimplePrincipalCollection();

		// allow easier access through principals.byType(PartyIdentifier.class)
        if (getId() != null && !getId().equals("") &&  !getId().equals("0")) {
            principals.add(new PartyIdentifier(getId()), DefaultRealm.REALM_NAME);
        }

		// allow easier access through principals.byType(EmailAddress.class)
		principals.add(new EmailAddress(email), DefaultRealm.REALM_NAME);

		return principals;
	}

    @Override
    public Set<PartyRole> getRoles() {
        return new HashSet<>(partyRoles);
    }

    @Override
    public void addRole(PartyRole role) {
        partyRoles.add((DefaultPartyRole) role);
        getEventDispatcher().dispatch(new RoleGrantEvent(role, this));
    }

    @Override
    public void removeRole(PartyRole role) {
        partyRoles.remove(role);
        getEventDispatcher().dispatch(new RoleRevokeEvent(role, this));
    }

    @Override
    public boolean hasRole(PartyRole role) {
        return partyRoles.contains(role);
    }

	@Override
    public Set<Preference> getPreferences() {
        return new HashSet<>(preferences.values());
    }

	public void setPreferences(Map<PreferenceType, Preference> preferences) {
		this.preferences = preferences;
	}

	@Override
    public Preference getPreference(PreferenceType type) {
        return preferences.get(type);
    }

    @Override
    public Preference getPreference(String type) {
        try {
            PreferenceType t = JPA.em().createNamedQuery("PreferenceType.findByName", PreferenceType.class)
                    .setParameter("name", type)
                    .getSingleResult();
            return getPreference(t);
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public void setPreference(Preference p) {
        preferences.put(p.getType(), p);
    }

    @Override
    public void setPreference(String type, String option) {
        PreferenceType t = null;

        // find or create a PreferenceType
        try {
            t = JPA.em().createNamedQuery("PreferenceType.findByName", PreferenceType.class)
                    .setParameter("name", type)
                    .getSingleResult();
        } catch (NoResultException e) {
            t = new PreferenceType(type);
        }

        // find or create a PreferenceOption
        PreferenceOption o = t.getOption(option);
        if (o == null) {
            o = new PreferenceOption(option);
        }

        setPreference(t, o);
    }

    @Override
    public void setPreference(PreferenceType t, PreferenceOption o) {
        setPreference(new Preference(t, o));
    }

    public boolean isLocked() {
        return isLocked;
    }

    public void setLocked(boolean locked) {
        isLocked = locked;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", this.getId())
                .append("name", this.getName())
                .append("email", this.getEmail())
                .toString();
    }
}
