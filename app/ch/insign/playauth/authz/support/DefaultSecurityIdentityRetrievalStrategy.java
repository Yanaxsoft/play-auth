package ch.insign.playauth.authz.support;

import ch.insign.playauth.authz.SecurityIdentity;
import ch.insign.playauth.authz.SecurityIdentityRetrievalStrategy;
import ch.insign.playauth.party.Party;
import ch.insign.playauth.party.PartyRole;

public class DefaultSecurityIdentityRetrievalStrategy implements SecurityIdentityRetrievalStrategy {

	@Override
	public SecurityIdentity getSecurityIdentity(Object authority) {

		if (authority == null) {
			return SecurityIdentity.ALL;
		}

		if (authority instanceof SecurityIdentity) {
			return (SecurityIdentity) authority;
		}

		if (authority instanceof Party) {
			return new SecurityIdentity((Party) authority);
		}

		if (authority instanceof PartyRole) {
			return new SecurityIdentity((PartyRole) authority);
		}

		throw new IllegalArgumentException("authority must be instance of Party or PartyRole or SecurityIdentity");
	}
}