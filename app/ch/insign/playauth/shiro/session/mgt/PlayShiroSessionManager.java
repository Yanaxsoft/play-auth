package ch.insign.playauth.shiro.session.mgt;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import org.apache.shiro.session.mgt.SessionManager;

public interface PlayShiroSessionManager extends SessionManager {

}
