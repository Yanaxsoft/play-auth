package ch.insign.playauth.party.preference;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@Embeddable
public class PreferenceOption implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column
    private String name;

    @Column
    private String description;

    protected PreferenceOption() {
    }

    public PreferenceOption(String name) {
        this(name, "");
    }

    public PreferenceOption(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(5, 37)
                .append(name)
                .toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        PreferenceOption other = (PreferenceOption) obj;
        return new EqualsBuilder()
                .append(name, other.name)
                .append(description, other.description)
                .isEquals();
    }

    @Override
    public String toString() {
        return name;
    }
}
