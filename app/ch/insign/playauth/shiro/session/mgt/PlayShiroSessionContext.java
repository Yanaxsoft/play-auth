package ch.insign.playauth.shiro.session.mgt;

import org.apache.shiro.session.mgt.SessionContext;

import play.mvc.Http;

public interface PlayShiroSessionContext extends SessionContext, ch.insign.playauth.shiro.util.HttpContextSource {

    @Override
    Http.Context getHttpContext();

    void setHttpContext(Http.Context context);

}
