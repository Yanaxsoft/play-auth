package ch.insign.playauth.event;

import ch.insign.playauth.authz.DomainPermission;
import ch.insign.playauth.authz.SecurityIdentity;

public abstract class PermissionAuthorizationEvent extends AuthorizationEvent {

    private final SecurityIdentity sid;
    private final DomainPermission<?> permission;

    public PermissionAuthorizationEvent(SecurityIdentity sid, DomainPermission<?> permission) {
        this.sid = sid;
        this.permission = permission;
    }

    public SecurityIdentity getSid() {
        return sid;
    }

    public DomainPermission<?> getPermission() {
        return permission;
    }
}
