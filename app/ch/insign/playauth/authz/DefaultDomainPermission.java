package ch.insign.playauth.authz;

import java.util.Objects;


public class DefaultDomainPermission<T> implements DomainPermission<T> {
	private final String domain;
	private final String name;
	private final ObjectIdentity target;

	public DefaultDomainPermission(String domain, String name, ObjectIdentity target) {
		Objects.requireNonNull(domain);
		Objects.requireNonNull(name);
		Objects.requireNonNull(target);
		this.domain = domain;
		this.name = name;
		this.target = target;
	}

	@Override
	public String domain() {
		return domain;
	}

	@Override
	public String name() {
		return name;
	}

	@Override
	public ObjectIdentity target() {
		return target;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof DomainPermission)) return false;

		DomainPermission that = (DomainPermission) o;

		if (!domain.equals(that.domain())) return false;
		if (!name.equals(that.name())) return false;
		if (!target.equals(that.target())) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = domain.hashCode();
		result = 31 * result + name.hashCode();
		result = 31 * result + target.hashCode();
		return result;
	}
}
