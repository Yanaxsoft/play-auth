package ch.insign.playauth.authz;

import org.apache.shiro.crypto.hash.Hash;

/**
 * A AuthorizationHash is a unique representation of the set of permissions.
 *
 *
 * If several parties have equal AuthorizationHash-es, then they're also equally treated by the Authorizer.
 *
 * The AuthorizationHash might be used as cache key to store authorization requests as for
 * the same AuthorizationHash the Authorizer will always return the same result.
 */
public interface AuthorizationHash extends Hash {
}
