package ch.insign.playauth.event;

import ch.insign.playauth.party.Party;

public class PartyStartImpersonationEvent implements EventObject {

    private final Party originalParty;
    private final Party otherParty;

    public PartyStartImpersonationEvent(Party originalParty, Party otherParty) {
        this.originalParty = originalParty;
        this.otherParty = otherParty;
    }

    public Party getOriginalParty() {
        return originalParty;
    }

    public Party getOtherParty() {
        return otherParty;
    }
}
