package ch.insign.playauth.authz;

public interface AuthorizationHashRetrievalStrategy {
	AuthorizationHash getAuthorizationHash(SecurityIdentity target);
}
