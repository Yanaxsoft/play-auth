package ch.insign.playauth.party;

import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.subject.PrincipalCollection;

import java.util.Collection;

/**
 * TODO: refactor create(..) methods
 * TODO: add possibility to manage subtypes of Party.class
 */
public interface PartyManager {

    Party find(Object identifier);

    Party findOneByPrincipal(Object principal);

    Party findOneByPrincipals(PrincipalCollection principals);

    Collection<Party> findAll();

    Class<? extends Party> getPartyClass();

    Party create(String name, AuthenticationInfo authInfo);

    Party create(String name, Collection<Object> principals, Object credentials);

    Party create(String name, Object credentials, Object... principals);

    Party create(String name, PrincipalCollection principals, Object credentials);

    void delete(Party party);

    void save(Party party);
}
