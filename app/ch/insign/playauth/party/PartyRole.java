package ch.insign.playauth.party;

public interface PartyRole {

	String getId();

    String getName();
}
