package ch.insign.playauth.shiro.mgt;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import ch.insign.playauth.shiro.session.mgt.DefaultPlayShiroSessionManager;
import ch.insign.playauth.shiro.session.mgt.PlayShiroSessionKey;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.session.mgt.SessionContext;
import org.apache.shiro.session.mgt.SessionKey;
import org.apache.shiro.subject.SubjectContext;
import play.mvc.Http;

import java.io.Serializable;
import java.util.Collection;

public class DefaultPlayShiroSecurityManager extends DefaultSecurityManager implements PlayShiroSecurityManager {
	private final static Logger logger = LoggerFactory.getLogger(DefaultPlayShiroSecurityManager.class);

    public DefaultPlayShiroSecurityManager() {
        super();
        setSubjectFactory(new DefaultPlayShiroSubjectFactory());
        setRememberMeManager(new ch.insign.playauth.shiro.mgt.CookieRememberMeManager());
        setSessionManager(new DefaultPlayShiroSessionManager());
    }

    public DefaultPlayShiroSecurityManager(Realm singleRealm) {
        this();
        setRealm(singleRealm);
    }

    public DefaultPlayShiroSecurityManager(Collection<Realm> realms) {
        this();
        setRealms(realms);
    }

    @Override
    protected SubjectContext createSubjectContext() {
        return new ch.insign.playauth.shiro.subject.support.DefaultPlayShiroSubjectContext();
    }

    @Override
    protected SubjectContext copy(SubjectContext subjectContext) {
        if (subjectContext instanceof ch.insign.playauth.shiro.subject.PlayShiroSubjectContext) {
            return new ch.insign.playauth.shiro.subject.support.DefaultPlayShiroSubjectContext(
                    (ch.insign.playauth.shiro.subject.PlayShiroSubjectContext) subjectContext);
        }
        return super.copy(subjectContext);
    }

    @Override
    protected SessionContext createSessionContext(SubjectContext subjectContext) {
        SessionContext sessionContext = super
                .createSessionContext(subjectContext);

        if (subjectContext instanceof ch.insign.playauth.shiro.subject.PlayShiroSubjectContext) {
            ch.insign.playauth.shiro.subject.PlayShiroSubjectContext sc = (ch.insign.playauth.shiro.subject.PlayShiroSubjectContext) subjectContext;
            Http.Context context = sc.resolveHttpContext();
            ch.insign.playauth.shiro.session.mgt.DefaultPlayShiroSessionContext playShiroSessionContext = new ch.insign.playauth.shiro.session.mgt.DefaultPlayShiroSessionContext(sessionContext);

            if (context != null) {
                playShiroSessionContext.setHttpContext(context);
            }

            sessionContext = playShiroSessionContext;
        }

        return sessionContext;
    }

    @Override
    protected SessionKey getSessionKey(SubjectContext context) {
        if (ch.insign.playauth.shiro.util.HttpContextUtils.hasHttpContext(context)) {
            Serializable sessionId = context.getSessionId();
            Http.Context httpContext = ch.insign.playauth.shiro.util.HttpContextUtils.getHttpContext(context);
            return new PlayShiroSessionKey(sessionId, httpContext);
        } else {
            return super.getSessionKey(context);
        }
    }
}
