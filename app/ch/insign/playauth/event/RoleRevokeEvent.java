package ch.insign.playauth.event;

import ch.insign.playauth.party.Party;
import ch.insign.playauth.party.PartyRole;

public class RoleRevokeEvent extends RoleAuthorizationEvent {

	public RoleRevokeEvent(PartyRole role, Party party) {
		super(role, party);
	}
}
