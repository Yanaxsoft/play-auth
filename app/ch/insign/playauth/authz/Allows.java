package ch.insign.playauth.authz;

import java.lang.annotation.*;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Allows {
	String[] value();
}
