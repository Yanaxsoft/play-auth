package ch.insign.playauth.event;

import ch.insign.playauth.authz.DomainPermission;
import ch.insign.playauth.authz.SecurityIdentity;

public class PermissionDenyEvent extends PermissionAuthorizationEvent {

	public PermissionDenyEvent(SecurityIdentity sid, DomainPermission<?> permission) {
		super(sid, permission);
	}
}
