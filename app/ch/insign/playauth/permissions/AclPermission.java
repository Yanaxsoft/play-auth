package ch.insign.playauth.permissions;

import ch.insign.playauth.authz.DomainPermissionEnum;

public enum AclPermission implements DomainPermissionEnum<Object> {
	UPDATE_ACL;
}
