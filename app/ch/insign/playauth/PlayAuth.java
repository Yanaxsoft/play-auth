package ch.insign.playauth;

import ch.insign.playauth.authz.*;
import ch.insign.playauth.event.EventDispatcher;
import ch.insign.playauth.event.PartyStartImpersonationEvent;
import ch.insign.playauth.party.Party;
import ch.insign.playauth.party.PartyIdentifier;
import ch.insign.playauth.party.PartyManager;
import ch.insign.playauth.party.PartyRoleManager;
import com.google.common.base.Throwables;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.credential.CredentialsMatcher;
import org.apache.shiro.authc.credential.PasswordMatcher;
import org.apache.shiro.authc.credential.PasswordService;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.UnauthenticatedException;
import org.apache.shiro.mgt.RealmSecurityManager;
import org.apache.shiro.realm.AuthenticatingRealm;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.subject.support.SubjectThreadState;
import org.apache.shiro.util.ThreadContext;
import org.apache.shiro.util.ThreadState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.api.Play;
import play.api.inject.Injector;
import play.db.jpa.JPA;
import play.mvc.Http;

import java.util.Optional;
import java.util.function.Supplier;

/**
 * Provides access to authentication and authorization services.
 */
public class PlayAuth {

	private final static Logger logger = LoggerFactory.getLogger(PlayAuth.class);

	public static PlayAuthEnvironment env() {
		return injector().instanceOf(PlayAuthEnvironment.class);
	}

    public static EventDispatcher getEventDispatcher() {
        return injector().instanceOf(EventDispatcher.class);
    }

	public static AccessControlList getAccessControlList() {
		return injector().instanceOf(AccessControlList.class);
	}

	public static Authorizer getAuthorizer() {
		return injector().instanceOf(Authorizer.class);
	}

	public static AccessControlListVoter getAccessControlListVoter() {
		return injector().instanceOf(AccessControlListVoter.class);
	}

    public static AccessControlManager getAccessControlManager() {
		return injector().instanceOf(AccessControlManager.class);
    }

    public static PermissionManager getPermissionManager() {
		return injector().instanceOf(PermissionManager.class);
    }

    public static PartyManager getPartyManager() {
		return injector().instanceOf(PartyManager.class);
    }

    public static PartyRoleManager getPartyRoleManager() {
		return injector().instanceOf(PartyRoleManager.class);
    }

    public static ObjectIdentity getObjectIdentity(Object domainObject) {
        return injector().instanceOf(ObjectIdentityRetrievalStrategy.class)
				.getObjectIdentity(domainObject);
    }

    public static SecurityIdentity getSecurityIdentity(Object authority) {
		return injector().instanceOf(SecurityIdentityRetrievalStrategy.class)
				.getSecurityIdentity(authority);
    }

	public static AuthorizationHash getAuthorizationHash(Object authority) {
		return injector().instanceOf(AuthorizationHashRetrievalStrategy.class)
				.getAuthorizationHash(getSecurityIdentity(authority));
	}

    /**
     * Optionally returns a string value of party identifier.
     */
    public static Optional<String> getPartyIdentifier() {
        PartyIdentifier id = getSubject().getPrincipals().oneByType(PartyIdentifier.class);
        return id == null ? Optional.empty() : Optional.ofNullable(id.getIdentifier());
    }

    /**
     * Retrieves the Party by an identity of the currently accessible Subject
     */
    public static Party getCurrentParty() {
        if (!isAnonymous()) {
            return Optional.ofNullable(getPartyManager().findOneByPrincipals(getSubject().getPrincipals()))
                    .orElseGet(() -> {
                        getSubject().logout();
	                    logger.warn("Party is authenticated or remembered " +
			                    "but could not be retrieved by the principals from current session.");
                        return null;
                    });
        } else {
            return null;
        }
    }

	/**
	 * Returns the first PasswordMatcher occurrence found within the realms of
	 * the currently accessible SecurityManager.
	 */
	public static PasswordMatcher getPasswordMatcher() throws RuntimeException {
		PasswordMatcher passwordMatcher = null;

		RealmSecurityManager mgr = (RealmSecurityManager) env().getSecurityManager();

		for (Realm realm : mgr.getRealms()) {
			if (!(realm instanceof AuthenticatingRealm)) {
				continue;
			}

			CredentialsMatcher credentialsMatcher = ((AuthenticatingRealm) realm).getCredentialsMatcher();
			if (!(credentialsMatcher instanceof PasswordMatcher)) {
				continue;
			}

			passwordMatcher = (PasswordMatcher) credentialsMatcher;
			break;
		}

		if (passwordMatcher != null) {
			return passwordMatcher;
		} else {
			throw new RuntimeException(
					"No PasswordMatcher available among registered AuthenticatingRealm instances.");
		}
	}

	/**
	 * Returns the first PasswordService occurance found within the realms of
	 * the currently accessible SecurityManager.
	 */
	public static PasswordService getPasswordService() throws RuntimeException {
		try {
			return getPasswordMatcher().getPasswordService();
		} catch (RuntimeException cause) {
			throw new RuntimeException(
					"No PasswordService available among registered AuthenticatingRealm instances.", cause);
		}
	}

	/**
	 * Authenticate current subject by the given party's principals.
	 *
	 * The calling code must manually unbind the authenticated subject from the
	 * current thread by calling PlayAuth.unbind(), unless it is executing inside
	 * the scope of ch.insign.playauth.controllers.actions.WithSubjectAction.
	 */
    public static void authenticate(Party party, Http.Context ctx) {
        Subject subject = createSubject(party, ctx);
        subject.getSession().touch();
        ThreadState threadState = new SubjectThreadState(subject);
        threadState.bind();
    }

	public static void authenticate(Party party) {
		authenticate(party, Http.Context.current());
	}

	/**
	 * Binds a subject to the current thread.
	 */
	public static void bind(Http.Context ctx) {
		authenticate(null, ctx);
	}

	/**
	 * Removes a subject from current thread.
	 */
	public static void unbind() {
		ThreadContext.remove();
	}

    /**
     * Allows this Party to 'run as' or 'assume' another identity indefinitely.
     */
    public static void impersonate(Party party) throws NullPointerException, IllegalStateException {
        Party current = getCurrentParty();
        getSubject().runAs(party.getPrincipals());
        PlayAuth.getEventDispatcher().dispatch(new PartyStartImpersonationEvent(current, party));
    }

    /**
     * Returns {@code true} if this {@code Party} is 'impersonating' another identity other than its original one or
     * {@code false} otherwise (normal {@code Party} state).  See the {@link #impersonate impersonate} method for more
     * information.
     */
    public static boolean isImpersonated() {
        return getSubject().isRunAs();
    }

    /**
     * Releases the current 'impersonate' (assumed) identity and reverts back to the previous 'pre impersonate'
     * identity that existed before {@code #impersonate impersonate} was called.
     */
    public static Party endImpersonation() {
	    return Optional.ofNullable(getSubject().releaseRunAs())
			    .map(principals -> getPartyManager().findOneByPrincipals(principals))
			    .orElse(null);
    }

    /**
     * Returns the previous 'pre impersonate' identity of this {@code Party} before assuming the current
     * {@link #impersonate impersonate} identity, or {@code null} if this {@code Party} is not operating under an assumed
     * identity (normal state).
     */
    public static Party getPreviousParty() {
        if (isImpersonated() && !isAnonymous()) {
            return getPartyManager().findOneByPrincipals(getSubject().getPreviousPrincipals());
        } else {
            return null;
        }
    }

    public static void login(AuthenticationToken token) throws AuthenticationException {
        getSubject().login(token);
    }

    public static void logout() {
        getSubject().logout();
    }

    public static boolean isAuthenticated() {
        return getSubject().isAuthenticated();
    }

    public static boolean isRemembered() {
        return getSubject().isRemembered();
    }

    public static boolean isAnonymous() {
        return !(isAuthenticated() || isRemembered());
    }

	/**
	 * Returns {@code true} if anonymous party is granted the given permission
	 */
	public static boolean isRestricted(DomainPermission<?> permission) {
		return getAccessControlManager().isNotPermitted(SecurityIdentity.ALL, permission);
	}

	/**
	 * Returns {@code true} if anonymous party is granted the given permission on all objects of the given type.
	 */
	public static <T, V extends T> boolean isRestricted(DomainPermission<T> permission, Class<V> target) {
		return getAccessControlManager().isNotPermitted(SecurityIdentity.ALL, permission, target);
	}

	/**
	 * Returns {@code true} if anonymous party is granted the given permission on the given object.
	 */
	public static <T, V extends T> boolean isRestricted(DomainPermission<T> permission, V target) {
		return getAccessControlManager().isNotPermitted(SecurityIdentity.ALL, permission, target);
	}

    /**
     * Returns {@code true} if current party is granted the given permission.
     */
    public static boolean isPermitted(DomainPermission<?> permission) {
        return getAccessControlManager().isPermitted(getCurrentParty(), permission);
    }

	/**
	 * Returns {@code true} if current party is granted the given permission on all objects of the given type.
	 */
	public static <T, V extends T> boolean isPermitted(DomainPermission<T> permission, Class<V> target) {
		return getAccessControlManager().isPermitted(getCurrentParty(), permission, target);
	}

    /**
     * Returns {@code true} if current party is granted the given permission on the given object.
     */
    public static <T, V extends T> boolean isPermitted(DomainPermission<T> permission, V target) {
	    return getAccessControlManager().isPermitted(getCurrentParty(), permission, target);
    }

    /**
     * Throws {@code AuthorizationException} if current party is NOT granted the given permission.
     * @throws AuthorizationException
     */
    public static void requirePermission(DomainPermission<?> permission) throws AuthorizationException {
	    doAuthzCheck(() -> getAccessControlManager().requirePermission(getCurrentParty(), permission));
    }

	/**
	 * Throws {@code AuthorizationException} if current party is NOT granted the given permission on all objects of the given type.
	 * @throws AuthorizationException
	 */
	public static <T, V extends T> void requirePermission(DomainPermission<T> permission, Class<V> target) throws AuthorizationException {
		doAuthzCheck(() -> getAccessControlManager().requirePermission(getCurrentParty(), permission, target));
	}

    /**
     * Throws {@code AuthorizationException} if current party is NOT granted the given permission on the given object.
     * @throws AuthorizationException
     */
    public static <T, V extends T> void requirePermission(DomainPermission<T> permission, V target) throws AuthorizationException {
	    doAuthzCheck(() -> getAccessControlManager().requirePermission(getCurrentParty(), permission, target));
    }


    public static <T> T execute(Http.Context ctx, final Supplier<T> block) {
	    try {
		    return createSubject(null, ctx).execute(() -> {
			    try {
				    return block.get();
			    } catch (Throwable t) {
				    throw Throwables.propagate(Throwables.getRootCause(t));
			    }
		    });
	    } catch (Throwable t) {
		    throw Throwables.propagate(Throwables.getRootCause(t));
	    }
    }

	public static void execute(Http.Context ctx, final Runnable block) {
		try {
			createSubject(null, ctx).execute(() -> {
				try {
					block.run();
				} catch (Throwable t) {
					throw Throwables.propagate(Throwables.getRootCause(t));
				}
			});
		} catch (Throwable t) {
			throw Throwables.propagate(Throwables.getRootCause(t));
		}
	}

    public static <T> T executeAs(Party party, final Supplier<T> block) {
	    try {
		    return createSubject(party, null).execute(() -> {
			    try {
				    return block.get();
			    } catch (Throwable t) {
				    throw Throwables.propagate(Throwables.getRootCause(t));
			    }
		    });
	    } catch (Throwable t) {
		    throw Throwables.propagate(Throwables.getRootCause(t));
	    }
    }

	public static void executeAs(Party party, final Runnable block) {
		try {
			createSubject(party, null).execute(() -> {
				try {
					block.run();
				} catch (Throwable t) {
					throw Throwables.propagate(Throwables.getRootCause(t));
				}
			});
		} catch (Throwable t) {
			throw Throwables.propagate(Throwables.getRootCause(t));
		}
	}

    public static <T> T executeWithTransaction(Http.Context ctx, final Supplier<T> block) {
        return execute(ctx, () -> JPA.withTransaction(block));
    }

	public static void executeWithTransaction(Http.Context ctx, final Runnable block) {
		execute(ctx, () -> JPA.withTransaction(block));
	}

    public static <T> T executeWithTransactionAs(Party party, final Supplier<T> block) {
        return executeAs(party, () -> JPA.withTransaction(block));
    }

	public static void executeWithTransactionAs(Party party, final Runnable block) {
		executeAs(party, () -> JPA.withTransaction(block));
	}

	private static Injector injector() {
		return Play.current().injector();
	}

	private static Subject getSubject() {
		return env().getSubject();
	}

	private static Subject createSubject(Party party, Http.Context ctx) {
		return env().createSubject(party, ctx);
	}

	private static void doAuthzCheck(Runnable assertion) {
		try {
			assertion.run();
		} catch (UnauthenticatedException e) {
			throw Throwables.propagate(e);
		} catch (AuthorizationException e) {
			if (isAuthenticated()) {
				throw Throwables.propagate(e);
			} else {
				String msg = "This subject is anonymous - it does not have any identifying principals and " +
						"authorization operations require an identity to check against.  A Subject instance will " +
						"acquire these identifying principals automatically after a successful login is performed " +
						"be executing " + PlayAuth.class.getName() + ".login(AuthenticationToken) or when 'Remember Me' " +
						"functionality is enabled.  This exception can also occur when a " +
						"previously logged-in Subject has logged out which " +
						"makes it anonymous again.  Because an identity is currently not known due to any of these " +
						"conditions, authorization is denied.";

				throw new UnauthenticatedException(msg);
			}
		} catch (Throwable t) {
			throw Throwables.propagate(t);
		}
	}
}
