package ch.insign.playauth.party;

import java.io.Serializable;

/**
 * Represents an identifier that is unique within a given context.
 */
public interface UniqueIdentifier extends Serializable {

    String getIdentifier();
}
