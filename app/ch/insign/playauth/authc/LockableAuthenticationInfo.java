package ch.insign.playauth.authc;

import org.apache.shiro.authc.AuthenticationInfo;

public interface LockableAuthenticationInfo extends AuthenticationInfo {

    /**
     * Returns <code>true</code> if this AuthenticationInfo is locked and thus
     * cannot be used to login, <code>false</code> otherwise.
     */
    boolean isLocked();

    /**
     * Sets whether or not the AuthenticationInfo is locked and can be used to
     * login.
     */
    void setLocked(boolean locked);

}
