package ch.insign.playauth.authz;

import java.util.Set;

/**
 * The central point to manage domain permisions.
 */
public interface PermissionManager {

	void addAllowingRule(DomainPermission<?> left, DomainPermission<?> right);

	Set<DomainPermission<?>> resolveAllowingPermissions(DomainPermission<?> permission);

	Set<DomainPermission<?>> resolvePermissionsAllowedBy(DomainPermission<?> permission);

	boolean implies(DomainPermission<?> left, DomainPermission<?> right);

	boolean allows(DomainPermission<?> left, DomainPermission<?> right);

	DomainPermission<?> definePermission(Class<?> domainType, String name);

	Set<DomainPermission<?>> getDefinedPermissions();

	/**
	 * Returns set of {@link DomainPermission permissions} for which the given {@link ObjectIdentity oid} is applicable
	 * target ({@link PermissionManager#applyTarget})
	 */
	Set<DomainPermission<?>> getDefinedPermissions(ObjectIdentity oid);

	<T> Set<DomainPermission<T>> getDefinedPermissions(Class<T> targetType);

	Set<DomainPermission<?>> getAllowedPermissions(SecurityIdentity sid, ObjectIdentity oid);

	Set<DomainPermission<?>> getAllowedPermissions(SecurityIdentity sid);

	Set<DomainPermission<?>> getDeniedPermissions(SecurityIdentity sid, ObjectIdentity oid);

	Set<DomainPermission<?>> getDeniedPermissions(SecurityIdentity sid);

	DomainPermission<?> resolveDomainPermission(String domain, String action, ObjectIdentity target);

	/**
	 * Returns a new DomainPermission with the given target applied.
	 */
	<T, V extends T> DomainPermission<T> applyTarget(DomainPermission<T> p, V target);

	/**
	 * Returns a new DomainPermission with the given target applied.
	 */
	<T, V extends T> DomainPermission<T> applyTarget(DomainPermission<T> p, Class<V> target);

	/**
	 * Returns a new DomainPermission with the given target applied.
	 */
	<T> DomainPermission<T> applyTarget(DomainPermission<T> p, ObjectIdentity target);

	/**
	 * Returns true if the given target can be applied to the given permission.
	 */
	boolean isTargetApplicable(DomainPermission<?> p, ObjectIdentity target);

	/**
	 * Returns the qualified name of the underlying DomainPermission.
	 *
	 * The qualified name is composed of the 'qualifier' and the 'local name' which are separated with dot (".").
	 */
	String getQualifiedName(DomainPermission<?> p);
}
