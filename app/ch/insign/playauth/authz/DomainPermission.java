package ch.insign.playauth.authz;

/**
 * A DomainPermission represents the ability to perform an action or access a resource within specific domain.
 * @param <T> The base type of the permission domain.
 */
public interface DomainPermission<T> {

	/**
	 * This is a superuser (root) permission that implies all other permissions.
	 */
	public static final DomainPermission<Object> ALL = new DomainPermission<Object>() {
		@Override public String domain() { return "*"; }
		@Override public String name() { return "*"; }
		@Override public ObjectIdentity target() { return ObjectIdentity.ALL; }
	};

	/**
	 * Returns a domain name.
	 */
	String domain();

	/**
	 * Returns a statement that defines an explicit behavior or action that can applied to the given target.
	 */
	String name();

	/**
	 * Returns the ObjectIdentity of the target object to which this permission applies.
	 */
	ObjectIdentity target();
}

