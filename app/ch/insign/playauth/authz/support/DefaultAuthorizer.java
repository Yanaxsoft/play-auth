package ch.insign.playauth.authz.support;

import ch.insign.playauth.authz.AccessControlListVoter;
import ch.insign.playauth.authz.Authorizer;
import ch.insign.playauth.authz.DomainPermission;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

public class DefaultAuthorizer implements Authorizer {

	private final static Logger logger = LoggerFactory.getLogger(DefaultAuthorizer.class);

	private final AccessControlListVoter aclVoter;

	@Inject
	public DefaultAuthorizer(AccessControlListVoter aclVoter) {
		this.aclVoter = aclVoter;
	}

	public boolean isPermitted(Object authority, DomainPermission<?> permission) {
		return aclVoter
				// the DomainPermission.ALL has highest priority
				.vote(authority, DomainPermission.ALL)
				.map(AccessControlListVoter.Vote::allowed)
				.orElseGet(() ->
					aclVoter
					.vote(authority, permission)
					.map(AccessControlListVoter.Vote::allowed)
					.orElse(false));
	}
}
