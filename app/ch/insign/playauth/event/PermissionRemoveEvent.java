package ch.insign.playauth.event;

import ch.insign.playauth.authz.DomainPermission;
import ch.insign.playauth.authz.SecurityIdentity;

public class PermissionRemoveEvent extends PermissionAuthorizationEvent {

	public PermissionRemoveEvent(SecurityIdentity sid, DomainPermission<?> permission) {
		super(sid, permission);
	}
}
