package ch.insign.playauth.permissions;

import ch.insign.playauth.authz.DomainPermissionEnum;
import ch.insign.playauth.party.Party;


public enum PartyPermission implements DomainPermissionEnum<Party> {
	BROWSE,
	READ,
	EDIT,
	ADD,
	DELETE,
	IMPERSONATE,
    EDIT_PASSWORD,
    REQUEST_PASSWORD_RESET;
}
