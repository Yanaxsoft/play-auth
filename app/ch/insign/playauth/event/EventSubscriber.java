package ch.insign.playauth.event;

import java.util.Collection;

public interface EventSubscriber extends EventListener {
    Collection<Class<? extends EventObject>> getSubscribedEvents();
}
