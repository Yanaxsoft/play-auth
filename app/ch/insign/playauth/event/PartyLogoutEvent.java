package ch.insign.playauth.event;

import ch.insign.playauth.party.Party;

public class PartyLogoutEvent implements EventObject {

    private final Party party;

    public PartyLogoutEvent(Party party) {
        this.party = party;
    }

    public Party getParty() {
        return party;
    }
}
