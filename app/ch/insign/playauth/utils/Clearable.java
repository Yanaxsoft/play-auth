package ch.insign.playauth.utils;

public interface Clearable {
    void clear();
}
