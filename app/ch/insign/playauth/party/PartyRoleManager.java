package ch.insign.playauth.party;

import java.util.Collection;

public interface PartyRoleManager {

    PartyRole find(Object identifier);

    PartyRole findOneByName(String name);

    Collection<PartyRole> findByName(String name);

    Collection<PartyRole> findAll();

    PartyRole create(String name);

    void delete(PartyRole role);

    public void save(PartyRole role);
}
