package ch.insign.playauth.party;

public enum PartyType {

    /** Represents information about human geing */
    PERSON,

    /** Represents administrative and functional structure */
    ORGANIZATION
}
