package ch.insign.playauth.controllers;

import play.api.mvc.Call;


public interface RouteResolver {
    public Call login();
    public Call logout(String backURL);

    public Call listParties();
    public Call editParty(String id);
    public Call updateParty(String id);
    public Call listRole(String id);
    public Call updatePartyRoles(String id);
    public Call changePassword(String id);
    public Call doChangePassword(String id);

    public Call createParty();
    public Call saveParty();
}
