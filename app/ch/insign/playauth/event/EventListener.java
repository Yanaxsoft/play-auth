package ch.insign.playauth.event;

public interface EventListener {
    void handle(EventObject event);
}
