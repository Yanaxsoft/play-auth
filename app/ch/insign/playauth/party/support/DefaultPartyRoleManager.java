package ch.insign.playauth.party.support;

import play.db.jpa.JPA;
import ch.insign.playauth.party.PartyRole;
import ch.insign.playauth.party.PartyRoleManager;
import com.uaihebert.factory.EasyCriteriaFactory;
import com.uaihebert.model.EasyCriteria;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class DefaultPartyRoleManager implements PartyRoleManager {

	private final static Logger logger = LoggerFactory.getLogger(PartyRoleManager.class);

    @Override
    public PartyRole find(Object identifier) {
	    if (!isPrimaryKey(identifier)) {
		    return null;
	    }

	    final Long id = Long.parseLong(String.valueOf(identifier));

        return em().find(DefaultPartyRole.class, id);
    }

    @Override
    public PartyRole findOneByName(String name) {
        try {
            return query()
                    .andEquals("name", name)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public Collection<PartyRole> findByName(String name) {
        Collection<PartyRole> roles = new ArrayList<>();
        try {
            List<? extends DefaultPartyRole> results =
                    query().andStringLike("name", name).getResultList();
            roles.addAll(results);
            return roles;
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public Collection<PartyRole> findAll() {
        return new ArrayList<>(query().getResultList());
    }

    @Override
    public PartyRole create(String name) {
        PartyRole role = new DefaultPartyRole(name);
        return getOrCreate(role);
    }

    @Override
    public void delete(PartyRole r) {
        PartyRole role = findOneByName(r.getName());
        if (role != null) {
            em().remove(role);
        }
    }

    @Override
    public void save(PartyRole role) {
        if (em().contains(role)) {
            em().merge(role);
        } else {
            em().persist(role);
        }
    }

    /*
     * HELPERS
     */

    private EntityManager em() {
        return JPA.em();
    }

    private PartyRole getOrCreate(PartyRole r) {
        if (em().contains(r)) {
            return r;
        }

        PartyRole role = findOneByName(r.getName());
        if (role != null) {
            //TODO: merge with PartyRole r
            return role;
        } else {
            em().persist(r);
            return r;
        }
    }

	private boolean isPrimaryKey(Object value) {
		try {
			return Long.parseLong(String.valueOf(value)) >= 0;
		} catch (NumberFormatException e) {
			return false;
		}
	}

    public EasyCriteria<? extends DefaultPartyRole> query() {
        return EasyCriteriaFactory.createQueryCriteria(JPA.em(), DefaultPartyRole.class);
    }

}
