package ch.insign.playauth.controllers.actions;

import ch.insign.playauth.PlayAuth;
import ch.insign.playauth.authz.AuthorizationHandler;
import com.google.common.base.Throwables;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.UnauthenticatedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.api.Play;
import play.mvc.Action;
import play.mvc.Http.Context;
import play.mvc.Result;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

public class WithSubjectAction<T> extends Action<T>  {

	private static final Logger logger = LoggerFactory.getLogger(WithSubjectAction.class);

    private static final String WITH_SUBJECT_FLAG = "WITH_SUBJECT";

    @Override
    public CompletionStage<Result> call(Context ctx) {
        // Skip thread state binding if the wrapped action is already aware of subject
        if (isWithSubject(ctx)) {
            return doCall(ctx);
        }

        try {
            // bind subject to current thread
            PlayAuth.bind(ctx);
            ctx.args.put(WITH_SUBJECT_FLAG, true);

            try {
	            try {
		            return CompletableFuture.completedFuture(doCall(ctx).toCompletableFuture().get());
	            } catch (Throwable t) {
		            throw unwrapped(t);
	            }
            } catch (UnauthenticatedException e) {
                // Attempting to execute an authorization action when a
                // successful authentication hasn't yet occurred
                return CompletableFuture.completedFuture(getHandler().onUnauthenticated(ctx, e));
            } catch (AuthorizationException e) {
                // Access to a requested resource is not allowed
                return CompletableFuture.completedFuture(getHandler().onUnauthorized(ctx, e));
            } catch (Throwable t) {
                throw Throwables.propagate(t);
            }

        } finally {
            // unbind subject from current thread
            ctx.args.put(WITH_SUBJECT_FLAG, false);
            PlayAuth.unbind();
        }
    }

    public CompletionStage<Result> doCall(Context ctx)  {
        return delegate.call(ctx);
    }

    private Boolean isWithSubject(Context ctx) {
        return ctx.args.containsKey(WITH_SUBJECT_FLAG) && (Boolean) ctx.args.get(WITH_SUBJECT_FLAG);
    }

    private AuthorizationHandler getHandler() {
        return Play.current().injector().instanceOf(AuthorizationHandler.class);
    }

	private static Throwable unwrapped(Throwable t) {
		if (!(t instanceof AuthorizationException)) {
			Throwable cause = Throwables.getRootCause(t);
			if (cause instanceof AuthorizationException) {
				return cause;
			}
		}
		return t;
	}
}
