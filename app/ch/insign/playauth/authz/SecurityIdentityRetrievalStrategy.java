package ch.insign.playauth.authz;

public interface SecurityIdentityRetrievalStrategy {
	SecurityIdentity getSecurityIdentity(Object authority);
}
