package ch.insign.playauth.authz;

import java.io.Serializable;
import java.util.Collection;
import java.util.Optional;

public interface Hierarchy extends Serializable {
	Optional<Object> getParent(Object obj);
	Collection<Object> getChildren(Object obj);
}
