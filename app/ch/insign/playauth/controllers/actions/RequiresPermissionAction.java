package ch.insign.playauth.controllers.actions;

import play.db.jpa.JPA;
import ch.insign.playauth.PlayAuth;
import ch.insign.playauth.authz.DomainPermission;
import play.mvc.Http;
import play.mvc.Result;

import java.util.concurrent.CompletionStage;

/**
 * Ensures the calling subject has required {@link RequiresPermissionAction#permission(Http.Context ctx) permission}.
 *
 * Example usage:
 * {@code
 *   public class RequiresEditPartyPermissionAction extends RequiresPermissionAction<Void> {
 *       @Override
 *       public DomainPermission<?> permission(Http.Context ctx) {
 *           // define class-wide permission
 *           DomainPermission<Party> perm = PartyPermission.EDIT;
 *
 *           // retrieve a specific target to which permission applies
 *           String id = ctx.request().getQueryString("id");
 *           Party party = PlayAuth.getPartyManager().find(id);
 *
 *           // return party-specific permission or class-wide permission if party is not found
 *           return party != null ? PlayAuth.getPermissionManager().applyTarget(perm, party) : perm;
 *       }
 *   }
 * }
 */
abstract public class RequiresPermissionAction<T> extends WithSubjectAction<T> {

	@Override
	public CompletionStage<Result> doCall(Http.Context ctx) {
		JPA.withTransaction(() -> PlayAuth.requirePermission(permission(ctx)));
		return this.delegate.call(ctx);
	}

	abstract public DomainPermission<?> permission(Http.Context ctx);
}
