package ch.insign.playauth.shiro.subject;

import org.apache.shiro.subject.SubjectContext;

import play.mvc.Http;

import ch.insign.playauth.shiro.util.HttpContextSource;

public interface PlayShiroSubjectContext extends HttpContextSource, SubjectContext {

    @Override
    Http.Context getHttpContext();

    void setHttpContext(Http.Context context);

    Http.Context resolveHttpContext();

}
