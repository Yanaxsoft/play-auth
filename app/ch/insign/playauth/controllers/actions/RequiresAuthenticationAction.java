package ch.insign.playauth.controllers.actions;

import ch.insign.playauth.PlayAuth;
import org.apache.shiro.authz.UnauthenticatedException;
import play.mvc.Http;
import play.mvc.Result;

import java.util.concurrent.CompletionStage;

/**
 * Ensures the calling subject is {@link ch.insign.playauth.PlayAuth#isAuthenticated() authenticated}.
 */
public class RequiresAuthenticationAction extends WithSubjectAction<RequiresAuthentication> {

	@Override
	public CompletionStage<Result> doCall(Http.Context ctx) {
		if (!PlayAuth.isAuthenticated()) {
			throw new UnauthenticatedException("The current Subject is not authenticated. Access denied.");
		}

		return this.delegate.call(ctx);
	}
}
