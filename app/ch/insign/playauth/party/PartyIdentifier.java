package ch.insign.playauth.party;


import java.util.Objects;

public class PartyIdentifier implements UniqueIdentifier {

    private static final long serialVersionUID = 1L;

    private String identifier;

	public PartyIdentifier(Party party) {
		Objects.requireNonNull(party);
		identifier = party.getId();
	}

    public PartyIdentifier(String id) {
	    Objects.requireNonNull(id);
	    identifier = id;
    }

    @Override
    public String getIdentifier() {
        return identifier;
    }

    @Override
    public String toString() {
        return identifier;
    }
}
