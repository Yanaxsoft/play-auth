package ch.insign.playauth.authz;

import com.google.common.collect.ImmutableMap;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public interface ObjectIdentityMetadata {

	<T> Optional<T> get(Class<T> type);


	public static class Builder {
		private Map<Class<?>, Object> metadata = new HashMap<>();

		public <T, V extends T> Builder add(Class<T> type, V object) {
			metadata.put(type, object);
			return this;
		}

		public ObjectIdentityMetadata build() {

			Map<Class<?>, Object> meta = ImmutableMap.copyOf(metadata);

			return new ObjectIdentityMetadata() {
				@Override
				public <T> Optional<T> get(Class<T> type) {
					return Optional.ofNullable(meta.get(type)).filter(type::isInstance).map(type::cast);
				}
			};
		}
	}
}
