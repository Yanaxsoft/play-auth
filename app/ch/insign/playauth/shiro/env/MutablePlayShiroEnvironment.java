package ch.insign.playauth.shiro.env;

import ch.insign.playauth.shiro.mgt.PlayShiroSecurityManager;

public interface MutablePlayShiroEnvironment extends PlayShiroEnvironment {
    void setPlayShiroSecurityManager(PlayShiroSecurityManager playShiroSecurityManager);
}
