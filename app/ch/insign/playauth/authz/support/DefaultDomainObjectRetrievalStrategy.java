package ch.insign.playauth.authz.support;

import play.db.jpa.JPA;
import ch.insign.playauth.authz.AccessControlEntry;
import ch.insign.playauth.authz.DomainObjectRetrievalStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.api.Play;

import javax.persistence.Entity;

public class DefaultDomainObjectRetrievalStrategy implements DomainObjectRetrievalStrategy {
	private final static Logger logger = LoggerFactory.getLogger(DefaultDomainObjectRetrievalStrategy.class);

	@Override
	public Object getSidObject(AccessControlEntry ace) throws UnknownDomainClassException {
		return retrieveObject(ace.getSid().getType(), ace.getSid().getIdentifier());
	}

	@Override
	public Object getOidObject(AccessControlEntry ace) throws UnknownDomainClassException {
		return retrieveObject(ace.getOid().getType(), ace.getOid().getIdentifier());
	}

	private Object retrieveObject(String className, String identifier) throws UnknownDomainClassException {
		try {
			Class<?> clazz = Class.forName(className, true, Play.current().classloader());
			if (clazz.isAnnotationPresent(Entity.class)) {
				if ("*".equals(className) || "*".equals(identifier)) {
					throw new IllegalArgumentException( "It's not possible to retrieve an object " +
							"with \"*\" as a parameter for class name or identifier.");
				}
				long sidIdentifier = Long.parseLong(identifier);
				Object sidObject = JPA.em().find(clazz, sidIdentifier);
				return sidObject;
			}

			throw new UnknownDomainClassException("Given class is not an entity. " +
					"Don't know how to retrieve an object " +
					"for given class name and identifier from AccessControlEntry object");
		} catch (ClassNotFoundException e) {
			logger.warn("Class not found: {}. Possibly all entries in ACL with this class should be removed. ", className);
		} catch (NumberFormatException nfe) {
			logger.error("Can't cast identifier to integer: " + className);
		}
		return null;
	}
}
