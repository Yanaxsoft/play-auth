package ch.insign.playauth.authz.support;

import ch.insign.playauth.authz.*;
import ch.insign.playauth.utils.Clearable;
import com.google.common.base.Throwables;
import org.apache.commons.beanutils.BeanUtils;

import javax.inject.Inject;
import javax.inject.Provider;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class DefaultObjectIdentityRetrievalStrategy implements ObjectIdentityRetrievalStrategy, Clearable {

	private static final List<String> IDENTIFIER_PROPERTIES = Arrays.asList("id", "identifier");
	private final Map<Class<?>, ObjectIdentityMetadata> metadata = new ConcurrentHashMap<>();
	private final Provider<PermissionManager> pm;

	@Inject
	public DefaultObjectIdentityRetrievalStrategy(Provider<PermissionManager> pm) {
		this.pm = pm;
	}

	@Override
    public ObjectIdentity getObjectIdentity(Object target) {

        if (target == null) {
            return ObjectIdentity.ALL;
        }

        if (target instanceof ObjectIdentity) {
            return (ObjectIdentity) target;
        }

	    String type = resolveType(target);
	    String identifier = resolveIdentifier(target);
	    Map<Class<?>, Object> metadata = resolveMetadata(target);

	    return new ObjectIdentity(type, identifier, target, metadata);
    }

	@Override
	public Optional<ObjectIdentityMetadata> getMetadata(Class<?> type) {
		return Optional.ofNullable(metadata.get(type));
	}

	@Override
	public void setMetadata(Class<?> type, ObjectIdentityMetadata metadata) {
		this.metadata.put(type, metadata);
	}

	@Override
	public void clear() {
		metadata.clear();
	}

	private String resolveType(Object target) {
		if (DomainPermission.class.isAssignableFrom(getClassOf(target))) {
			return DomainPermission.class.getName();
		} else {
			return getClassOf(target).getName();
		}
	}

	private String resolveIdentifier(final Object target) {
		return getMetadata(getClassOf(target))
				.map(m -> m.get(Identifier.class))
				.orElseGet(() -> Optional.ofNullable(getClassOf(target).getAnnotation(WithIdentifier.class))
						.map(WithIdentifier::value)
						.map(clazz -> {
							try {
								return clazz.newInstance();
							} catch (InstantiationException cause) {
								throw new RuntimeException(
										"The class \"" + clazz.getName() + "\" must provide default no-argument constructor.", cause);
							} catch (Throwable t) {
								throw Throwables.propagate(t);
							}
						}))
				.flatMap(identifier -> identifier.getIdentifier(target))
				.filter(Objects::nonNull)
				.orElseGet(() -> {
					if (target instanceof Class<?>) {
						return ObjectIdentity.CLASS_IDENTIFIER;
					}

					if (target instanceof DomainPermission) {
						return pm.get().getQualifiedName((DomainPermission) target);
					} else {
						return IDENTIFIER_PROPERTIES.stream()
								.map(propName -> {
									try {
										return BeanUtils.getProperty(target, propName);
									} catch (NoSuchMethodException e) {
										return null;
									} catch (Throwable t) {
										throw Throwables.propagate(t);
									}
								})
								.filter(Objects::nonNull)
								.findFirst()
								.orElseThrow(() -> new NullPointerException("Could not extract identity from object " + target));
					}
				});
	}


	private Map<Class<?>, Object> resolveMetadata(final Object target) {
		Map<Class<?>, Object> data = new HashMap<>();

		getMetadata(getClassOf(target))
				.map(m -> m.get(Authorizer.class))
				.orElseGet(() -> resolveAuthorizer(target))
				.ifPresent(authorizer -> data.put(Authorizer.class, authorizer));

		getMetadata(getClassOf(target))
				.map(m -> m.get(Hierarchy.class))
				.orElseGet(() -> resolveHierarchy(target))
				.ifPresent(hierarchy -> data.put(Hierarchy.class, hierarchy));

		return data;
	}

	private Optional<Authorizer> resolveAuthorizer(final Object target) {
		return Optional.ofNullable(getClassOf(target).getAnnotation(WithAuthorizer.class))
				.map(WithAuthorizer::value)
				.map(clazz -> {
					try {
						return clazz.newInstance();
					} catch (InstantiationException cause) {
						throw new RuntimeException(
								"The class \"" + clazz.getName() + "\" must provide default no-argument constructor.", cause);
					} catch (Throwable t) {
						throw Throwables.propagate(t);
					}
				});
	}

	private Optional<Hierarchy> resolveHierarchy(final Object target) {
		return Optional.ofNullable(getClassOf(target).getAnnotation(WithHierarchy.class))
				.map(WithHierarchy::value)
				.map(clazz -> {
					try {
						return clazz.newInstance();
					} catch (InstantiationException cause) {
						throw new RuntimeException(
								"The class \"" + clazz.getName() + "\" must provide default no-argument constructor.", cause);
					} catch (Throwable t) {
						throw Throwables.propagate(t);
					}
				});
	}

	private Class<?> getClassOf(Object obj) {
		return obj instanceof Class<?> ? (Class<?>) obj : obj.getClass();
	}

}
