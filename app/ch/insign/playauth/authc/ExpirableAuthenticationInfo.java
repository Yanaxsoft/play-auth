package ch.insign.playauth.authc;

import org.apache.shiro.authc.AuthenticationInfo;

public interface ExpirableAuthenticationInfo extends AuthenticationInfo {

    /**
     * Returns whether or not the AuthenticationInfo's credentials are expired.
     * This usually indicates that the Subject or an application administrator
     * would need to change the credentials before the AuthenticationInfo could
     * be used.
     */
    boolean isCredentialsExpired();

    /**
     * Sets whether or not the AuthenticationInfo's credentials are expired. A
     * <code>true</code> value indicates that the Subject or application
     * administrator would need to change their credentials before the
     * AuthenticationInfo could be used.
     */
    void setCredentialsExpired(boolean credentialsExpired);

}
