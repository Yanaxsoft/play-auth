package ch.insign.playauth.authz;

import java.util.Collection;
import java.util.Optional;

public interface AccessControlListVoter {

	/**
	 * Optionally returns a vote for the given authorization request.
	 */
	Optional<Vote> vote(Object authority, DomainPermission<?> permission);

	/**
	 * Optionally returns an inherited vote on the given permission for a single SecurityIdentity.
	 */
	Optional<Vote> inheritedVote(SecurityIdentity authority, DomainPermission<?> permission);

	/**
	 * Optionally reduces multiple votes into a single vote.
	 */
	Optional<Vote> reduce(Collection<Vote> votes);

	/**
	 * The Vote describes the response on the authorization request.
	 */
	public static class Vote {
		private final SecurityIdentity sid;
		private final DomainPermission<?> permission;
		private final boolean allowed;

		public Vote(AccessControlEntry ace, DomainPermission<?> permission) {
			this(ace.getSid(), permission, ace.allows(permission));
		}

		public Vote(SecurityIdentity sid, DomainPermission<?> permission, boolean allowed) {
			this.sid = sid;
			this.permission = permission;
			this.allowed = allowed;
		}

		public SecurityIdentity sid() {
			return sid;
		}

		public DomainPermission<?> permission() {
			return permission;
		}

		public boolean allowed() {
			return allowed;
		}
	}
}
