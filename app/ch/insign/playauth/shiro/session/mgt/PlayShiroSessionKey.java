package ch.insign.playauth.shiro.session.mgt;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import ch.insign.playauth.shiro.session.PlayShiroSession;
import org.apache.shiro.session.mgt.DefaultSessionKey;
import play.mvc.Http;

import java.io.Serializable;

public class PlayShiroSessionKey extends DefaultSessionKey implements ch.insign.playauth.shiro.util.HttpContextSource {
	private final static Logger logger = LoggerFactory.getLogger(PlayShiroSessionKey.class);

    private static final long serialVersionUID = -510753928849558123L;

    private final Http.Context context;

    public PlayShiroSessionKey(Serializable sessionId, Http.Context context) {
        this(context);
        if (null != sessionId) {
            setSessionId(sessionId);
        }
    }

    public PlayShiroSessionKey(Http.Context context) {
        this.context = context;
        if (context != null) {
            setSessionId(PlayShiroSession.getId(context.session()));
        } else {
            setSessionId(PlayShiroSession.generateId());
        }
    }

    @Override
    public Http.Context getHttpContext() {
        return context;
    }
}
