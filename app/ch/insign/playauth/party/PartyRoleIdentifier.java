package ch.insign.playauth.party;


import java.util.Objects;

public class PartyRoleIdentifier  implements UniqueIdentifier {

    private static final long serialVersionUID = 1L;

    private String identifier;

	public PartyRoleIdentifier(PartyRole role) {
		Objects.requireNonNull(role);
		identifier = role.getId();
	}


	public PartyRoleIdentifier(String id) {
		Objects.requireNonNull(id);
        identifier = id;
    }

    @Override
    public String getIdentifier() {
        return identifier;
    }

    @Override
    public String toString() {
        return identifier;
    }
}
