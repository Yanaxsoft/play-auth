package ch.insign.playauth.party.preference;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.persistence.*;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@Entity
@Table(name = "auth_preference_type")
@NamedQueries({
        @NamedQuery(
                name = "PreferenceType.findAll",
                query = "SELECT t FROM PreferenceType t"),
        @NamedQuery(
                name = "PreferenceType.findByName",
                query = "SELECT t FROM PreferenceType t WHERE t.name = :name"),
})
public class PreferenceType {

    @Id
    @GeneratedValue
    private long id;

    @Column(unique = true, nullable = false)
    private String name;

    @Column
    private String description;

    @ElementCollection(fetch = FetchType.EAGER, targetClass = PreferenceOption.class)
    @CollectionTable(name = "auth_preference_type_options")
    @OneToMany(cascade = CascadeType.ALL)
    @MapKey(name = "name")
    private Map<String, PreferenceOption> options = new HashMap<String, PreferenceOption>();

    protected PreferenceType() {

    }

    public PreferenceType(String name) {
        this(name, "");
    }

    public PreferenceType(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Set<PreferenceOption> getOptions() {
        return new HashSet<>(options.values());
    }

    public PreferenceOption getOption(String name) {
        return options.get(name);
    }

    public void addOption(PreferenceOption option) {
        options.put(option.getName(), option);
    }

    public void addOption(String name) {
        addOption(new PreferenceOption(name));
    }

    public void addOption(String name, String description) {
        addOption(new PreferenceOption(name, description));
    }

    public void removeOption(PreferenceOption option) {
        removeOption(option.getName());
    }

    public void removeOption(String name) {
        options.remove(name);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(7, 19)
                .append(name)
                .toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        PreferenceType other = (PreferenceType) obj;
        return new EqualsBuilder()
                .append(name, other.name)
                .append(description, other.description)
                .append(options, other.options)
                .isEquals();
    }

    @Override
    public String toString() {
        return name + options.values().toString();
    }
}
