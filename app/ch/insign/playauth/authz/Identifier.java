package ch.insign.playauth.authz;

import java.io.Serializable;
import java.util.Optional;

public interface Identifier extends Serializable {
	Optional<String> getIdentifier(Object obj);
}