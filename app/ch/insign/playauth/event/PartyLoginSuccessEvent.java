package ch.insign.playauth.event;

import ch.insign.playauth.party.Party;
import org.apache.shiro.authc.AuthenticationToken;

public class PartyLoginSuccessEvent implements EventObject {

    private final AuthenticationToken token;
    private final Party party;

    public PartyLoginSuccessEvent(AuthenticationToken token, Party party) {
        this.token = token;
        this.party = party;
    }

    public AuthenticationToken getToken() {
        return token;
    }

    public Party getParty() {
        return party;
    }
}
