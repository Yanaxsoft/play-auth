name := "play-auth"

organization := "ch.insign"

version := "1.5.2"

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  javaJpa,
  cache,
  "javax.enterprise" % "cdi-api" % "1.1",
  "org.apache.shiro" % "shiro-core" % "1.2.5",
  "org.apache.commons" % "commons-lang3" % "3.2.1",
  "org.apache.xbean" % "xbean-finder" % "3.7",
  "org.reflections" % "reflections" % "0.9.9",
  "uaihebert.com" % "EasyCriteria" % "3.0.0",
  "org.eclipse.persistence" % "eclipselink" % "2.6.2"
)

resolvers += Resolver.bintrayRepo("insign", "play-cms")

// Change max-filename-length for scala-Compiler. Longer filenames (not sure what the threshold is) causes problems
// with encrypted home directories under ubuntu
scalacOptions ++= Seq("-Xmax-classfile-name", "100")

scalacOptions += "-feature"

licenses += ("Apache-2.0", url("https://www.apache.org/licenses/LICENSE-2.0.html"))
bintrayRepository := "play-cms"
bintrayOrganization := Some("insign")
publishMavenStyle := true

lazy val auth = (project in file("."))
  .enablePlugins(PlayJava)
