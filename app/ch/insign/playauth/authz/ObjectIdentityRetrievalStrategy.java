package ch.insign.playauth.authz;

import java.util.Optional;

public interface ObjectIdentityRetrievalStrategy {

	ObjectIdentity getObjectIdentity(Object target);

	Optional<ObjectIdentityMetadata> getMetadata(Class<?> type);

	void setMetadata(Class<?> type, ObjectIdentityMetadata metadata);
}
