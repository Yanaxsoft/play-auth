package ch.insign.playauth.event;

import ch.insign.playauth.party.Party;
import ch.insign.playauth.party.PartyRole;

public abstract class RoleAuthorizationEvent extends AuthorizationEvent {

    private final PartyRole role;
    private final Party party;

    public RoleAuthorizationEvent(PartyRole role, Party party) {
        this.role = role;
        this.party = party;
    }

    public PartyRole getRole() {
        return role;
    }

    public Party getParty() {
        return party;
    }
}
