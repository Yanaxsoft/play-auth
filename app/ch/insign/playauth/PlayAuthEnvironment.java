package ch.insign.playauth;

import ch.insign.playauth.authc.AuthenticationListener;
import ch.insign.playauth.party.Party;
import ch.insign.playauth.shiro.env.IniPlayShiroEnvironment;
import ch.insign.playauth.shiro.mgt.PlayShiroSecurityManager;
import ch.insign.playauth.shiro.subject.PlayShiroSubject;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.UnavailableSecurityManagerException;
import org.apache.shiro.authc.AbstractAuthenticator;
import org.apache.shiro.authc.Authenticator;
import org.apache.shiro.mgt.AuthenticatingSecurityManager;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.mvc.Http;

import javax.inject.Inject;
import javax.inject.Singleton;


@Singleton
public class PlayAuthEnvironment {

    private final static Logger logger = LoggerFactory.getLogger(PlayAuthEnvironment.class);

    private final PlayShiroSecurityManager securityManager;

    @Inject
    public PlayAuthEnvironment() {
        securityManager = initSecurityManager();

        registerAuthenticationListener();
    }

    /**
     * Get the low level PlayShiroSecurityManager
     */
    public SecurityManager getSecurityManager() {
        return securityManager;
    }

    public Subject getSubject() {
        return getSubject(Http.Context.current.get());
    }

    public Subject getSubject(Http.Context ctx) {
        try {
            return SecurityUtils.getSubject();
        } catch (UnavailableSecurityManagerException e) {
            logger.trace("There is no accessible Subject, tyring to creating new Subject with optional Http.Context...", e);
            return createSubject(null, ctx);
        }
    }

    /**
     * Creates authenticated subject by the given principals or anonymous subject if no principals were given.
     */
    public Subject createSubject(Party party, Http.Context ctx) {
        PlayShiroSubject.Builder builder =
                new PlayShiroSubject.Builder(securityManager);

        if (party != null) {
            builder.principals(party.getPrincipals()).authenticated(true);
        }

        if (ctx != null) {
            builder.httpContext(ctx);
        }

        return builder.buildPlayShiroSubject();
    }

    private PlayShiroSecurityManager initSecurityManager() {
        IniPlayShiroEnvironment shiroEnvironment = new IniPlayShiroEnvironment();
        shiroEnvironment.init();

        return shiroEnvironment.getPlayShiroSecurityManager();
    }

    private void registerAuthenticationListener() {
        Authenticator authenticator = ((AuthenticatingSecurityManager) securityManager).getAuthenticator();
        if (authenticator instanceof AbstractAuthenticator) {
            ((AbstractAuthenticator) authenticator).getAuthenticationListeners().add(new AuthenticationListener());
        }
    }
}
