package ch.insign.playauth.shiro.config;

import ch.insign.playauth.shiro.mgt.DefaultPlayShiroSecurityManager;
import org.apache.shiro.config.Ini;
import org.apache.shiro.config.IniSecurityManagerFactory;
import org.apache.shiro.mgt.SecurityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PlayShiroIniSecurityManagerFactory extends IniSecurityManagerFactory  {
	private final static Logger logger = LoggerFactory.getLogger(PlayShiroIniSecurityManagerFactory.class);

    public PlayShiroIniSecurityManagerFactory() {
        super();
    }

    public PlayShiroIniSecurityManagerFactory(Ini config) {
        super(config);
    }

    @Override
    protected SecurityManager createDefaultInstance() {
        return new DefaultPlayShiroSecurityManager();
    }
}
