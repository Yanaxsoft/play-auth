package ch.insign.playauth.controllers.actions;

import ch.insign.playauth.PlayAuth;
import org.apache.shiro.authz.UnauthenticatedException;
import play.mvc.Http;
import play.mvc.Result;

import java.util.concurrent.CompletionStage;

/**
 * Ensures the calling subject is either {@link ch.insign.playauth.PlayAuth#isAuthenticated() authenticated}
 * or {@link ch.insign.playauth.PlayAuth#isRemembered() remembered}.
 */
public class RequiresUserAction extends WithSubjectAction<RequiresUser> {

	@Override
	public CompletionStage<Result> doCall(Http.Context ctx) {
		if (PlayAuth.isAnonymous()) {
			throw new UnauthenticatedException("Attempting to perform a user-only operation.  The current Subject is " +
					"not a user (they haven't been authenticated or remembered from a previous login).  " +
					"Access denied.");
		}

		return this.delegate.call(ctx);
	}
}
