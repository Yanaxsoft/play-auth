package ch.insign.playauth.authz;

import ch.insign.playauth.party.Party;
import ch.insign.playauth.party.PartyRole;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.shiro.util.StringUtils;

import javax.persistence.Embeddable;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Optional;

@Embeddable
public class SecurityIdentity implements Serializable {

    public static final SecurityIdentity ALL = new SecurityIdentity("*", "*");
    public static final SecurityIdentity UNKNOWN = new SecurityIdentity("?", "?");

    private String type;
    private String identifier;

	@Transient
	private Object source;

    protected SecurityIdentity() {

    }

    public SecurityIdentity(String type, String identifier) {
        if (!StringUtils.hasText(type)) {
            throw new IllegalArgumentException("type required");
        }
        if (!StringUtils.hasText(identifier)) {
            throw new IllegalArgumentException("identifier required");
        }

        this.type = type;
        this.identifier = identifier;
    }

    public SecurityIdentity(Class<?> type, String identifier) {
        if (type == null) {
            throw new IllegalArgumentException("type required");
        }
        if (!StringUtils.hasText(identifier)) {
            throw new IllegalArgumentException("identifier required");
        }

        this.type = type.getName();
        this.identifier = identifier;
    }

    public SecurityIdentity(Party party) {
        if (party == null) {
            throw new IllegalArgumentException("party cannot be null");
        }

        type = party.getClass().getName();
        identifier = party.getId();
	    source = party;
    }

    public SecurityIdentity(PartyRole role) {
        if (role == null) {
            throw new IllegalArgumentException("role cannot be null");
        }

        type = role.getClass().getName();
        identifier = role.getId();
	    source = role;
    }

    public String getType() {
        return type;
    }

    public String getIdentifier() {
        return identifier;
    }

	/**
	 * TODO: try to resolve source if not present
	 */
	public Optional<Object> getSource() {
		return Optional.ofNullable(source);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		SecurityIdentity that = (SecurityIdentity) o;

		if (!identifier.equals(that.identifier)) return false;
		if (!type.equals(that.type)) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = type.hashCode();
		result = 31 * result + identifier.hashCode();
		return result;
	}

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("type", type)
                .append("identifier", identifier)
                .toString();
    }
}
