package ch.insign.playauth.views.helper

import ch.insign.playauth.PlayAuth.{getObjectIdentity => oid, getPermissionManager => pm}
import ch.insign.playauth.authz.{DefaultDomainPermission, DomainPermission, ObjectIdentity}

class RichDomainPermission[T](domain: String, name: String, target: ObjectIdentity) extends DefaultDomainPermission[T](domain, name, target) {

  def apply[V <: T](target: V) = pm.applyTarget(this.asInstanceOf[DomainPermission[Any]], oid(target))
  def apply[V <: T](target: Class[V]) = pm.applyTarget(this.asInstanceOf[DomainPermission[Any]], oid(target))
  def apply(target: ObjectIdentity) = pm.applyTarget(this.asInstanceOf[DomainPermission[Any]], target)
  def id() = pm.getQualifiedName(this)

}