package ch.insign.playauth;

import ch.insign.playauth.authz.*;
import ch.insign.playauth.authz.support.*;
import ch.insign.playauth.event.EventDispatcher;
import ch.insign.playauth.party.PartyManager;
import ch.insign.playauth.party.PartyRoleManager;
import ch.insign.playauth.party.support.DefaultPartyManager;
import ch.insign.playauth.party.support.DefaultPartyRoleManager;
import play.api.Configuration;
import play.api.Environment;
import play.api.inject.Binding;
import play.api.inject.Module;
import scala.collection.Seq;

import javax.inject.Singleton;

public class PlayAuthModule extends Module {
	public Seq<Binding<?>> bindings(Environment environment, Configuration configuration) {
		return seq(
				bind(PlayAuthEnvironment.class).toSelf().in(Singleton.class),
				bind(EventDispatcher.class).toSelf().in(Singleton.class),

				bind(PermissionManager.class).to(DefaultPermissionManager.class),
				bind(AccessControlManager.class).toSelf(),

				bind(Authorizer.class).to(DefaultAuthorizer.class),
				bind(AccessControlListVoter.class).to(DefaultAccessControlListVoter.class),

				bind(AuthorizationHashRetrievalStrategy.class).to(DefaultAuthorizationHashRetrievalStrategy.class),
				bind(DomainObjectRetrievalStrategy.class).to(DefaultDomainObjectRetrievalStrategy.class),
				bind(ObjectIdentityRetrievalStrategy.class).to(DefaultObjectIdentityRetrievalStrategy.class),
				bind(SecurityIdentityRetrievalStrategy.class).to(DefaultSecurityIdentityRetrievalStrategy.class),

				bind(PartyManager.class).to(DefaultPartyManager.class),
				bind(PartyRoleManager.class).to(DefaultPartyRoleManager.class),

				bind(AuthorizationHandler.class).to(DefaultAuthorizationHandler.class)
		);
	}
}
